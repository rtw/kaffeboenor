<?php

$PName = "";
$PDescription = "";
$PImgFileName = "";
$PPrice = "";
$UnitWeight = "";
$InStockQuantity = "";

$err = false;

if (isset($_POST["submit"])) {
  if (isset($_POST["PName"])) $PName = $_POST["PName"];
  if (isset($_POST["PDescription"])) $PDescription = $_POST["PDescription"];
  if (isset($_POST["PImgFileName"])) $PImgFileName = $_POST["PImgFileName"];
  if (isset($_POST["PPrice"])) $PPrice = $_POST["PPrice"];
  if (isset($_POST["UnitWeight"])) $UnitWeight = $_POST["UnitWeight"];
  if (isset($_POST["InStockQuantity"])) $InStockQuantity = $_POST["InStockQuantity"];

  if (!empty($PName) && !empty($PDescription) && !empty($PImgFileName) && !empty($PPrice)
  && !empty($UnitWeight) && !empty($InStockQuantity)) {
    require_once("db.php");
   

    $sql = "insert into product(PName, PDescription, PImgFileName, PPrice, UnitWeight, InStockQuantity)
            values('$PName', '$PDescription', '$PImgFileName', $PPrice, $UnitWeight, $InStockQuantity)";
    $result = $mydb->query($sql);
    if ($result == 1) {
        // popup review successful please login 
      echo "<p>The product has been added!</p>";
    } else {
      echo "<p>There was an error. Check spelling and try again.<p>";
    }
  } else {
    $err = true;
  }
}
?>

<!doctype html>

<head>
  <title>Add Product</title>
  <link rel="stylesheet" href="styles.css" />
  <style>
    .errlabel {
      color: red;
    }
  </style>
</head>

<body>
 
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
      <li><a href="rtw-cLogin.php">Profile</a></li>
      <li><a href="st-orderhistory.php">Order History</a></li>
      <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Add Product</h2> 
<p>
<a href="ras-employeeHome.php"><button class="btn btn-primary" type="button">Employee Product Home</button></a>
  </p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" 
  <p>
  <label>Product Name:
    <input type="text" name="PName" value="<?php echo $PName; ?>" />
    <?php
    if ($err && empty($PName)) {
      echo "<label class='errlabel'>Error: Please enter the product name.</label>";
    }
    ?>
    </label>
    <br />
  </p>
    
    <label>Product Description:
      <input type="text" name="PDescription" value="<?php echo $PDescription; ?>" />
      <?php
      if ($err && empty($PDescription)) {
        echo "<label class='errlabel'>Error: Please enter the product description.</label>";
      }
      ?>
    </label>
    <br />
    <p>
    <label>Product Image File Name (including the .jpg or .jpeg):
      <input type="text" name="PImgFileName" value="<?php echo $PImgFileName; ?>" />
      <?php
      if ($err && empty($PImgFileName)) {
        echo "<label class='errlabel'>Error: Please enter the name of your image file.</label>";
      }
      ?>
    </label>
    <br />
    </p>
    <label>Product Price:
      <input type="text" name="PPrice" value="<?php echo $PPrice; ?>" />
      <?php
      if ($err && empty($PPrice)) {
        echo "<label class='errlabel'>Error: Please enter the product price.</label>";
      }
      ?>
    </label>
    <br />
    <p>
    <label>Unit Weight:
      <input type="text" name="UnitWeight" value="<?php echo $UnitWeight; ?>" />
      <?php
      if ($err && empty($UnitWeight)) {
        echo "<label class='errlabel'>Error: Please enter weight of each unit.</label>";
      }
      ?>
    </label>
    <br />
    </p>
    <label> Stock Quantity:
      <input type="text" name="InStockQuantity" value="<?php echo $InStockQuantity; ?>" />
      <?php
      if ($err && empty($InStockQuantity)) {
        echo "<label class='errlabel'>Error: Please enter quantity that are being added.</label>";
      }
      ?>
    </label>
    <br />

    <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
 
    </section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br>
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>

                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
