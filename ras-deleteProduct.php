

<!doctype html>
<html>
<head>
  <title>Delete Product</title>
 
  <link rel="stylesheet" href="styles.css" />
  <style>
    table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgb(0, 139, 207);
    }
    td {
      height: 20px;
      color: white;
      background-color: rgb(80, 104, 140);
    }
  </style>
  <script src="jquery-3.1.1.min.js"></script>
  <script>
    //ajax in Javascript
		var asyncRequest;

    function getAllProducts() {
      //display all products
      var url = "ras-displayProducts.php";
        try {
          asyncRequest = new XMLHttpRequest();

          asyncRequest.onreadystatechange=stateChange;
          asyncRequest.open('GET',url,true);
          asyncRequest.send();
        }
          catch (exception) {alert("Request failed");}
    }

    
		function stateChange() {
			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				document.getElementById("contentArea").innerHTML=
					asyncRequest.responseText;
			}
		}

    function clearPage(){
      document.getElementById("contentArea").innerHTML = "";
    }

    function init(){

      var z3 = document.getElementById("productLink");
      z3.addEventListener("mouseover", getAllProducts);

      var z4 = document.getElementById("productLink");
      z4.addEventListener("mouseout", clearPage);

    

    }
    document.addEventListener("DOMContentLoaded", init);

    //ajax in jQuery
    $(function(){
      $("#productDropDown").change(function(){
        $.ajax({
          url:"ras-displayProducts.php?id=" + $("#productDropDown").val(),
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      })
      $("#deletebutton").click(function(){
       $.ajax({
         
          url:"ras-deleteproductdatabase.php?PID="+ $("#productDropDown").val() ,
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      }) 
    })
      
     
   
    
	</script>
</head>
<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
      <li><a href="rtw-cLogin.php">Profile</a></li>
      <li><a href="st-orderhistory.php">Order History</a></li>
      <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Delete Product</h2> 
<p>
<a href="ras-employeeHome.php"><button class="btn btn-primary" type="button">Employee Product Home</button></a>
</p>
<br>
  <a id="productLink" href="">View All Products</a> <br />
  <label> Search for a Product to delete: &nbsp;&nbsp;
    <select name="PID" id="productDropDown"  >
      <?php
        require_once("db.php");
        $sql = "select PID from product order by PID";
        $result = $mydb->query($sql);
        
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["PID"]."'>".$row["PID"]."</option>";
          
        }
      ?>
      
      
    </select>
  </label><br />
  <div id="contentArea">&nbsp;</div>
 
  <input type="submit" id='deletebutton' name="delete" value= Delete />
  </section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>

                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>





