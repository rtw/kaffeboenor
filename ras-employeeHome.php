<!doctype html>

<head>
  <title>Kaffe Boenor Employee Home</title>
  
    <meta charset="utf-8" />
    <meta author="Raquel Sorto"/> 
    <link rel="stylesheet" href="styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    
</head>
<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
        <li><a href="rtw-cLogin.php">Profile</a></li>
        <li><a href="st-orderhistory.php">Order History</a></li>
        <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Employee Product Home</h2> 
<a href="ras-kn-employeeMain.php"><button  type="button">Return to Employee Main</button></a>
  <div class="container-fluid">
 <br>
  <a href="ras-addProduct.php"><button class="btn btn-primary" type="button">ADD PRODUCT</button></a>

  <a href="ras-deleteProduct.php"><button class="btn btn-primary" type="button">DELETE PRODUCT</button></a>
 
  <a href="ras-modifySelection.php"><button class="btn btn-primary" type="button">MODIFY PRODUCT</button></a>

  <a href="ras-index.php"><button class="btn btn-primary" type="button">ANALYZE STOCK</button></a>
    

</div>
<br> 

</section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>

