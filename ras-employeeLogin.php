<?php
session_start();
$EID = 0;
$Email = "";
$EPassword = "";
$error = false;
$loginOK = null;

if (isset($_POST["submit"])) {
  if(isset($_POST["Email"])) $Email=$_POST["Email"];
  if(isset($_POST["EPassword"])) $EPassword=$_POST["EPassword"];

  if(empty($Email) || empty($EPassword)) {
    $error=true;
  }
  if($error==true){
    if(empty($Email)){
      echo"<p>Enter your email<p>";
    }
    if(empty($EPassword)){
      echo"<p>Enter your password<p>";
    }
  }

  if (!$error) {
    require_once("db.php");
    $sql = "select Email, EPassword from employee where Email= '$Email' and EPassword='$EPassword'";
    $result = $mydb->query($sql);

    $row=mysqli_fetch_array($result);

    if ($row){
      if (strcmp($Email, $row["Email"]) == 0 && strcmp($EPassword, $row["EPassword"]) == 0) {
        $loginOK = true;
      } else {
        $loginOK = false;
      }

      if($loginOK) {
        session_start();
        $_SESSION["EPassword"] = $EPassword;
        Header("Location: ras-kn-employeeMain.php");
      
      }

    } 
  }

}
?>
<!doctype html>
<head>
  <title>Kaffe Boenor Employee Login</title>
  <link rel="stylesheet" href="styles.css" />
  
</head>
<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
        <li><a href="rtw-cLogin.php">Profile</a></li>
        <li><a href="st-orderhistory.php">Order History</a></li>
        <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Employee Login</h2> 

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <label>Email:
      <input type="text" name="Email" value="<?php
        if(!empty($Email))
          echo $Email;
      ?>" />
    </label><br />
    <label>Password:
      <input type="password" name="EPassword" value="<?php
        if(!empty($EPassword))
          echo $EPassword;
      ?>" />
    </label><br />
    <input type="submit" name="submit" value="Login" class="btn btn-primary" />
    
    </section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
