<?php
session_start();
    $PID = 0;
    if(isset($_GET["PID"])) $PID=$_GET["PID"];
    
    require_once("db.php");
    $sql = "select * from product where PID = '$PID'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);


    $PNameU = $row['PName'];
    $PDescriptionU = $row['PDescription'];
    $PImgFileNameU = $row['PImgFileName'];
    $PPriceU = $row['PPrice'];
    $UnitWeightU = $row['UnitWeight'];
    $InStockQuantityU = $row['InStockQuantity'];
   
    $err = false;

    /* $PNameU = "";
    $PDescriptionU = "";
    $PImgFileNameU = "";
    $PPriceU = "";
    $UnitWeightU = "";
    $InStockQuantityU = ""; */

    if (isset($_POST["submit"])) {
        if (isset($_POST["PName"])) $PNameU = $_POST["PName"];
        if (isset($_POST["PDescription"])) $PDescriptionU = $_POST["PDescription"];
        if (isset($_POST["PImgFileName"])) $PImgFileNameU = $_POST["PImgFileName"];
        if (isset($_POST["PPrice"])) $PPriceU = $_POST["PPrice"];
        if (isset($_POST["UnitWeight"])) $UnitWeightU = $_POST["UnitWeight"];
        if (isset($_POST["InStockQuantity"])) $InStockQuantityU = $_POST["InStockQuantity"];
        if (isset($_POST["pid"])) $PID = $_POST["pid"];


        if (!empty($PNameU) && !empty($PDescriptionU)  && !empty($PPriceU) 
        && !empty($UnitWeightU) && !empty($InStockQuantityU)) {
            
            $sql = "update product set PName = '$PNameU', PDescription = '$PDescriptionU',
             PImgFileName = '$PImgFileNameU', PPrice = $PPriceU,
             UnitWeight = $UnitWeightU, InStockQuantity = $InStockQuantityU 
                  where PID = $PID";
                 
            $result = $mydb->query($sql);
            if ($result == 1) {
                echo "<p>Update Successful.</p>";
            } else {
                echo "<p>Update Failed<p>";
            }
        } else {
            $err = true;
        }
    }
?>

<!doctype html>

<head>
    <title>Product Modification</title>
    <link rel="stylesheet" href="styles.css" />
    <style>
        .errlabel {
            color: red;
        }
    </style>
</head>

<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
            <li><a href="st-store.php">Home</a></li>
            <li><a href="ras-employeeLogin.php">Employee Login</a></li>
            <li><a href="st-login.php">Customer Login</a></li>
            <li><a href="st-orderhistory.php">Order History</a></li>
            <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Modify Product</h2> 
<p>
<a href="ras-employeeHome.php"><button class="btn btn-primary" type="button">Employee Product Home</button></a>
</p>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" 
        <p>
        <label> Product Name:
        <input type="text" name="PName" value="<?php echo $PNameU; ?>" />
        <?php
        if ($err && empty($PNameU)) {
            echo "<label class='errlabel'>Error: Please enter a first name.</label>";
        }
        ?>
        </label>
        <br />
        </p>
        <label>Product Description:
            <input type="text" name="PDescription" value="<?php echo $PDescriptionU; ?>" />
            <?php
            if ($err && empty($PDescriptionU)) {
                echo "<label class='errlabel'>Error: Please enter a Last name.</label>";
            }
            ?>
        </label>
        <br />
        <p>
        <label>Product Image:
            <input type="text" name="PImgFileName" value="<?php echo $PImgFileNameU; ?>" />
            <?php
            //if ($err && empty($PImgFileName)) {
              //  echo "<label class='errlabel'>Error: Please enter an Email.</label>";
            //}
            ?>
        </label>
        <br />
        </p>
        <label>Product Price:
            <input type="text" name="PPrice" value="<?php echo $PPriceU; ?>" />
            <?php
            if ($err && empty($PPriceU)) {
                echo "<label class='errlabel'>Error: Please enter a password.</label>";
            }
            ?>
        </label>
        <br />
        <p>
        <label>Unit Weight:
            <input type="text" name="UnitWeight" value="<?php echo $UnitWeightU; ?>" />
            <?php
            if ($err && empty($UnitWeightU)) {
                echo "<label class='errlabel'>Error: Please enter a street address.</label>";
            }
            ?>
        </label>
        <br />
        </p>
        <label>In Stock Quantity:
            <input type="text" name="InStockQuantity" value="<?php echo $InStockQuantityU; ?>" />
            <?php
            if ($err && empty($InStockQuantityU)) {
                echo "<label class='errlabel'>Error: Please enter a city.</label>";
            }
            ?>
        </label>
        <br />
        <input type="hidden" name="pid" value="<?php echo $PID;?>">

        <input type="submit" name="submit" value="Submit" />    
        </section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
