<?php
  require_once("db.php");
 ?>
<html>
<head>
  <title>Product Analysis</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="styles.css" />
    <script src="store.js" async></script>
</head>
<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
        <li><a href="rtw-cLogin.php">Profile</a></li>
        <li><a href="st-orderhistory.php">Order History</a></li>
        <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">
<h2 class="section-header">Modify Selection</h2> 
<a href="ras-employeeHome.php"><button class="btn btn-primary" type="button">Employee Product Home</button></a>
  <form method='get' action='ras-modifyProduct.php'>
    <select name='PID'>
    <?php
        require_once("db.php");
        $sql = "select PID from product";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["PID"]."'>".$row["PID"]."</option>";
        }
      ?>    
    </select>
    <input name='submit' type='submit' value = 'select' />

  </form>


  </section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
