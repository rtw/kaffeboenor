<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="styles.css" />
<title>Bar Chart</title>
<meta charset="utf-8">
<style>

.bar {
  fill: steelblue;
}

.bar:hover {
  fill: brown;
}

.axis--x path {
  display: none;
}

</style>
</head>
<body>
<header class="main-header">
    <nav class="main-nav nav">
        <ul>
        <li><a href="st-store.php">Home</a></li>
        <li><a href="rtw-cLogin.php">Profile</a></li>
        <li><a href="st-orderhistory.php">Order History</a></li>
        <li><a href="st-index.php">Order Analysis</a></li>
        </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
<section class="container content-section">

<a href="ras-employeeHome.php"><button class="btn btn-primary" type="button">Employee Product Home</button></a>
<a href="ras-index.php"><button class="btn btn-primary" type="button">Analyze Another Product</button></a>
<svg width="960" height="500"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);
console.log(y(3));
var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//d3.tsv("data.tsv", function(d) {
  //d.frequency = +d.frequency;
  //console.log(d);
  //return d;

d3.json("ras-getData.php?PID=<?php
  if(isset($_GET['PID'])) 
    echo $_GET['PID'];
  else
    echo "0";

?>", function(error, data) {
  if (error) throw error;

  data.forEach(function(d){
    d.letter = d.PName;
    d.frequency = +d.totalvalue;
  })


  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(4,"s"))
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");

  g.selectAll(".bar")
    .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("y", function(d) { return y(d.frequency); })
      .attr("width", x.bandwidth())
      .attr("height", function(d) { return height - y(d.frequency); });
});
</script>
<p class ="center">The y-axis value represents the revenue if all in stock quantity sold of selected product.</p> <br>
</section>
    <br> <br> <br> <br> <br> <br> <br> <br>  <br>  <br> 
    <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>

