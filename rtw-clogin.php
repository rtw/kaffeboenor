<!DOCTYPE html>
<html>

<head>
  <title>KaffeBonor | Login</title>
  <meta name="description" content="This is the description">
  <link rel="stylesheet" href="styles.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="store.js" async></script>

</head>

<body>
  <header class="main-header">
    <nav class="main-nav nav">
      <ul>
      <li><a href="st-store.php">Home</a></li>
      <li><a href="rtw-cLogin.php">Profile</a></li>
      <li><a href="st-orderhistory.php">Order History</a></li>
      <li><a href="st-index.php">Order Analysis</a></li>     
       
      </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
  </header>
  <section class="container content-section">
    <h2 class="section-header">Customer Login</h2>
    
      <?php

      //check if logged in
      session_start();
      if (isset($_SESSION['CID'])) {
        header("location:rtw-profile.php");
      }

      //establsih variables to be pulled from form
      $CID = 0;
      $Email = "";
      $CPassword = "";
      $error = false;
      $loginOK = null;
      //establish post variables on server
      if (isset($_POST["loginbutton"])) {
        if (isset($_POST["Email"])) $Email = $_POST["Email"];
        if (isset($_POST["CPassword"])) $CPassword = $_POST["CPassword"];

        //input checking
        if (empty($Email) || empty($CPassword)) {
          $error = true;
        }
        if ($error) {
          if (empty($Email)) {
            echo "<p>Enter An Email<p>";
          }
          if (empty($CPassword)) {
            echo "<p>Enter A Password<p>";
          }
        }

        //search DB for match
        if (!$error) {
          require_once("db.php");
          $sql = "select CID, Email, CPassword from customers where Email = '$Email' and CPassword = '$CPassword'";
          $result = $mydb->query($sql);

          $row = mysqli_fetch_array($result);

          if ($row) {
            if (strcmp($Email, $row["Email"]) == 0 && strcmp($CPassword, $row["CPassword"]) == 0) {
              $CID = $row["CID"];
              $loginOK = true;
            } else {
              $loginOK = false;
            }
          }
          //successful login establishes session varibale and redirects
          if ($loginOK) {
            $_SESSION['CID'] = $CID;
            header("location:st-store.php");
          } else echo "<script>alert('Login failed(email case sensitive)');</script>";
          $CPassword = "";
        }
      }


      ?>

        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
          <label>Email:
            <br />
            <input type="email" name="Email" value="<?php if (!empty($Email)) echo $Email; ?>" />
          </label><br />
          <label>Password:
            <br />
            <input type="password" name="CPassword" value="<?php if (!empty($CPassword)) echo $CPassword; ?>" />
          </label><br />
          <input type="submit" name="loginbutton" value="Login" />
          <br>
          <br>
          <a href="rtw-cregistration.php"><button class="btn btn-primary" type="button">Create an Account</button></a><br>
          <br>
          <a href="store.html"><button class="btn btn-primary" type="button">Back to Products</button></a><br>

  </section><br>
  <footer class="main-footer">
    <div class="container main-footer-container">
      <h3 class="band-name">KaffeBonor</h3>
      <ul class="nav footer-nav">
        <li>
          <a href="https://www.youtube.com" target="_blank">
            <img src="Images/YouTube Logo.png">
          </a>
        </li>
        <li>
          <a href="https://www.spotify.com" target="_blank">
            <img src="Images/Spotify Logo.png">
          </a>
        </li>
        <li>
          <a href="https://www.facebook.com" target="_blank">
            <img src="Images/Facebook Logo.png">
          </a>
        </li>
        <li><a href="ras-employeeLogin.php">Employee Login</a></li>
        <li><a href="ras-employeeLogin.php">Employee Login</a></li>
      </ul>
    </div>
  </footer>
</body>

</html>