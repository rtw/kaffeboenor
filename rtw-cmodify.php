<!DOCTYPE html>
<html>

<head>
    <title>KaffeBonor | Modify Profile</title>
    <meta name="description" content="This is the description">
    <link rel="stylesheet" href="styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="store.js" async></script>
</head>

<body>
    <header class="main-header">
        <nav class="main-nav nav">
            <ul>
            <li><a href="st-store.php">Home</a></li>
            <li><a href="rtw-cLogin.php">Profile</a></li>
            <li><a href="st-orderhistory.php">Order History</a></li>
            <li><a href="st-index.php">Order Analysis</a></li>
            </ul>
        </nav>
        <h1 class="band-name band-name-large">KaffeBonor</h1>
    </header>
    <section class="container content-section">
        <h2 class="section-header">Modify Account</h2>
        <div class="container-fluid">
            <?php
            // check if logged in
            session_start();
            if (isset($_SESSION['CID'])) {
                $CID = $_SESSION['CID'];
            } else {
                header("location:rtw-clogin");
            }
            //get cusotmer info from DB
            require_once("db.php");
            $sql = "select * from customers where CID = '$CID'";
            $result = $mydb->query($sql);
            $row = mysqli_fetch_array($result);
            //save DB values as PHP variables
            $LastName = $row['LastName'];
            $FirstName = $row['FirstName'];
            $Email = $row['Email'];
            $CPassword = $row['CPassword'];
            $ShippingStreetAddress = $row['ShippingStreetAddress'];
            $ShippingCity = $row['ShippingCity'];
            $ShippingState = $row['ShippingState'];
            $ShippingZip = $row['ShippingZip'];
            $err = false;
            //save form info as php variables when submitted
            if (isset($_POST["submitmodify"])) {
                if (isset($_POST["LastName"])) $LastName = $_POST["LastName"];
                if (isset($_POST["FirstName"])) $FirstName = $_POST["FirstName"];
                if (isset($_POST["Email"])) $Email = $_POST["Email"];
                if (isset($_POST["CPassword"])) $CPassword = $_POST["CPassword"];
                if (isset($_POST["ShippingStreetAddress"])) $ShippingStreetAddress = $_POST["ShippingStreetAddress"];
                if (isset($_POST["ShippingCity"])) $ShippingCity = $_POST["ShippingCity"];
                if (isset($_POST["ShippingState"])) $ShippingState = $_POST["ShippingState"];
                if (isset($_POST["ShippingZip"])) $ShippingZip = $_POST["ShippingZip"];

                if (!empty($LastName) && !empty($FirstName) && !empty($Email) && !empty($CPassword) && !empty($ShippingStreetAddress) && !empty($ShippingCity) && !empty($ShippingState) && !empty($ShippingZip)) {
                    // popup login successful please login 
                    $sql = "update customers set LastName = '$LastName', FirstName = '$FirstName', Email = '$Email', CPassword = '$CPassword', ShippingStreetAddress = '$ShippingStreetAddress', ShippingCity = '$ShippingCity', ShippingState = '$ShippingState', ShippingZip = '$ShippingZip'
                  where CID = $CID";
                    $result = $mydb->query($sql);
                    if ($result == 1) {
                        echo '<script>alert("Update Successful!")</script>';

                        header("location:rtw-profile");
                    } else {
                        echo '<script>alert("Update Failed")</script>';
                    }
                } else {
                    $err = true;
                }
            }
            ?>

            <!doctype html>

            <body>

                <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" <label>First Name:
                    <br />
                    <input type="text" name="FirstName" value="<?php echo $FirstName; ?>" />
                    <?php
                    if ($err && empty($FirstName)) {
                        echo "<br><label class='errlabel'>Error: Please enter a first name.</label>";
                    }
                    ?>
                    </label>
                    <br />
                    <label>Last Name:
                        <br />
                        <input type="text" name="LastName" value="<?php echo $LastName; ?>" />
                        <?php
                        if ($err && empty($LastName)) {
                            echo "<br><label class='errlabel'>Error: Please enter a Last name.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>Email:
                        <br />
                        <input type="email" name="Email" value="<?php echo $Email; ?>" />
                        <?php
                        if ($err && empty($Email)) {
                            echo "<br><label class='errlabel'>Error: Please enter an Email.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>Password:
                        <br />
                        <input type="password" name="CPassword" value="<?php echo $CPassword; ?>" />
                        <?php
                        if ($err && empty($CPassword)) {
                            echo "<br><label class='errlabel'>Error: Please enter a password.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>Street Address:
                        <br />
                        <input type="text" name="ShippingStreetAddress" value="<?php echo $ShippingStreetAddress; ?>" />
                        <?php
                        if ($err && empty($ShippingStreetAddress)) {
                            echo "<br><label class='errlabel'>Error: Please enter a street address.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>City:
                        <br />
                        <input type="text" name="ShippingCity" value="<?php echo $ShippingCity; ?>" />
                        <?php
                        if ($err && empty($ShippingCity)) {
                            echo "<br><label class='errlabel'>Error: Please enter a city.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>State:
                        <br />
                        <select name="ShippingState" id="stateDropDown">
                            <option value="AL"<?=$ShippingState == 'AL' ? ' selected="selected"' : '';?>>Alabama</option>
                            <option value="AK" <?=$ShippingState == 'AK' ? ' selected="selected"' : '';?>>Alaska</option>
                            <option value="AZ"<?=$ShippingState == 'AZ' ? ' selected="selected"' : '';?>>Arizona</option>
                            <option value="AR"<?=$ShippingState == 'AR' ? ' selected="selected"' : '';?>>Arkansas</option>
                            <option value="CA"<?=$ShippingState == 'CA' ? ' selected="selected"' : '';?>>California</option>
                            <option value="CO"<?=$ShippingState == 'CO' ? ' selected="selected"' : '';?>>Colorado</option>
                            <option value="CT"<?=$ShippingState == 'CT' ? ' selected="selected"' : '';?>>Connecticut</option>
                            <option value="DE"<?=$ShippingState == 'DE' ? ' selected="selected"' : '';?>>Delaware</option>
                            <option value="DC"<?=$ShippingState == 'DC' ? ' selected="selected"' : '';?>>District Of Columbia</option>
                            <option value="FL"<?=$ShippingState == 'FL' ? ' selected="selected"' : '';?>>Florida</option>
                            <option value="GA"<?=$ShippingState == 'GA' ? ' selected="selected"' : '';?>>Georgia</option>
                            <option value="HI"<?=$ShippingState == 'HI' ? ' selected="selected"' : '';?>>Hawaii</option>
                            <option value="ID"<?=$ShippingState == 'ID' ? ' selected="selected"' : '';?>>Idaho</option>
                            <option value="IL"<?=$ShippingState == 'IL' ? ' selected="selected"' : '';?>>Illinois</option>
                            <option value="IN"<?=$ShippingState == 'IN' ? ' selected="selected"' : '';?>>Indiana</option>
                            <option value="IA"<?=$ShippingState == 'IA' ? ' selected="selected"' : '';?>>Iowa</option>
                            <option value="KS"<?=$ShippingState == 'KS' ? ' selected="selected"' : '';?>>Kansas</option>
                            <option value="KY"<?=$ShippingState == 'KY' ? ' selected="selected"' : '';?>>Kentucky</option>
                            <option value="LA"<?=$ShippingState == 'LA' ? ' selected="selected"' : '';?>>Louisiana</option>
                            <option value="ME"<?=$ShippingState == 'ME' ? ' selected="selected"' : '';?>>Maine</option>
                            <option value="MD"<?=$ShippingState == 'MD' ? ' selected="selected"' : '';?>>Maryland</option>
                            <option value="MA"<?=$ShippingState == 'MA' ? ' selected="selected"' : '';?>>Massachusetts</option>
                            <option value="MI"<?=$ShippingState == 'MI' ? ' selected="selected"' : '';?>>Michigan</option>
                            <option value="MN"<?=$ShippingState == 'MN' ? ' selected="selected"' : '';?>>Minnesota</option>
                            <option value="MS"<?=$ShippingState == 'MS' ? ' selected="selected"' : '';?>>Mississippi</option>
                            <option value="MO"<?=$ShippingState == 'MO' ? ' selected="selected"' : '';?>>Missouri</option>
                            <option value="MT"<?=$ShippingState == 'MT' ? ' selected="selected"' : '';?>>Montana</option>
                            <option value="NE"<?=$ShippingState == 'NE' ? ' selected="selected"' : '';?>>Nebraska</option>
                            <option value="NV"<?=$ShippingState == 'NV' ? ' selected="selected"' : '';?>>Nevada</option>
                            <option value="NH"<?=$ShippingState == 'NH' ? ' selected="selected"' : '';?>>New Hampshire</option>
                            <option value="NJ"<?=$ShippingState == 'NJ' ? ' selected="selected"' : '';?>>New Jersey</option>
                            <option value="NM"<?=$ShippingState == 'NM' ? ' selected="selected"' : '';?>>New Mexico</option>
                            <option value="NY"<?=$ShippingState == 'NY' ? ' selected="selected"' : '';?>>New York</option>
                            <option value="NC"<?=$ShippingState == 'NC' ? ' selected="selected"' : '';?>>North Carolina</option>
                            <option value="ND"<?=$ShippingState == 'ND' ? ' selected="selected"' : '';?>>North Dakota</option>
                            <option value="OH"<?=$ShippingState == 'OH' ? ' selected="selected"' : '';?>>Ohio</option>
                            <option value="OK"<?=$ShippingState == 'OK' ? ' selected="selected"' : '';?>>Oklahoma</option>
                            <option value="OR"<?=$ShippingState == 'OR' ? ' selected="selected"' : '';?>>Oregon</option>
                            <option value="PA"<?=$ShippingState == 'PA' ? ' selected="selected"' : '';?>>Pennsylvania</option>
                            <option value="RI"<?=$ShippingState == 'RI' ? ' selected="selected"' : '';?>>Rhode Island</option>
                            <option value="SC"<?=$ShippingState == 'SC' ? ' selected="selected"' : '';?>>South Carolina</option>
                            <option value="SD"<?=$ShippingState == 'SD' ? ' selected="selected"' : '';?>>South Dakota</option>
                            <option value="TN"<?=$ShippingState == 'TN' ? ' selected="selected"' : '';?>>Tennessee</option>
                            <option value="TX"<?=$ShippingState == 'TX' ? ' selected="selected"' : '';?>>Texas</option>
                            <option value="UT"<?=$ShippingState == 'UT' ? ' selected="selected"' : '';?>>Utah</option>
                            <option value="VT"<?=$ShippingState == 'VT' ? ' selected="selected"' : '';?>>Vermont</option>
                            <option value="VA"<?=$ShippingState == 'VA' ? ' selected="selected"' : '';?>>Virginia</option>
                            <option value="WA"<?=$ShippingState == 'WA' ? ' selected="selected"' : '';?>>Washington</option>
                            <option value="WV"<?=$ShippingState == 'WV' ? ' selected="selected"' : '';?>>West Virginia</option>
                            <option value="WI"<?=$ShippingState == 'WI' ? ' selected="selected"' : '';?>>Wisconsin</option>
                            <option value="WY"<?=$ShippingState == 'WY' ? ' selected="selected"' : '';?>>Wyoming</option>
                        </select>
                        <?php
                        if ($err && empty($ShippingState)) {
                            echo "<br><label class='errlabel'>Error: Please enter a state.</label>";
                        }
                        ?>
                    </label>
                    <br />
                    <label>Zip Code:
                        <br />
                        <input name="ShippingZip" type="text" pattern="[0-9]*" value="<?php echo $ShippingZip; ?>">
                        <?php
                        if ($err && empty($ShippingZip)) {
                            echo "<label class='errlabel'>Error: Please enter a Zip Code.</label>";
                        }
                        ?>
                    </label>
                    <br />


                    <input type="submit" name="submitmodify" value="Submit" />
            </body>






    </section>

    <section class="container content-section">
        <a href="rtw-profile"><button class="btn btn-primary btn-back" type="button">Back to Profile</button></a>
    </section>
    <footer class="main-footer">
        <div class="container main-footer-container">
            <h3 class="band-name">KaffeBonor</h3>
            <ul class="nav footer-nav">
                <li>
                    <a href="https://www.youtube.com" target="_blank">
                        <img src="Images/YouTube Logo.png">
                    </a>
                </li>
                <li>
                    <a href="https://www.spotify.com" target="_blank">
                        <img src="Images/Spotify Logo.png">
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com" target="_blank">
                        <img src="Images/Facebook Logo.png">
                    </a>
                </li>
                <li><a href="ras-employeeLogin.php">Employee Login</a></li>
            </ul>
        </div>
    </footer>
</body>

</html>