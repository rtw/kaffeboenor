<?php
  require_once("db.php");

  $sql = " select PName as Product, avg(rating) as AverageRating from product
  inner join  review
  on product.pid = review.pid
  group by PName
  order by PName";

  $result = $mydb->query($sql);

  $data = array();
  for($x=0; $x<mysqli_num_rows($result); $x++) {
    $data[] = mysqli_fetch_assoc($result);
  }

  echo json_encode($data);
 ?>
