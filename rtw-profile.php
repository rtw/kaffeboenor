<!DOCTYPE html>
<html>

<head>
  <title>KaffeBonor | Product Page</title>
  <meta name="description" content="This is the description">
  <link rel="stylesheet" href="styles.css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="store.js" async></script>
  <script src="jquery-3.1.1.min.js"></script>
  <style>
    table,
    th,
    td {
      border: 1px solid black;
    }

    table {
      border-collapse: collapse;
      empty-cells: show;
    }

    th {
      color: white;
      background-color: rgb(0, 139, 207);
    }

    td {
      height: 20px;
      color: white;
      background-color: rgb(80, 104, 140);
    }
  </style>
  <script>
    //ajax in Javascript
    var asyncRequest;

    function getAllProducts() {
      //display all products
      var url = "rtw-getCproducts.php";
      try {
        asyncRequest = new XMLHttpRequest();

        asyncRequest.onreadystatechange = stateChange;
        asyncRequest.open('GET', url, true);
        asyncRequest.send();
      } catch (exception) {
        alert("Request failed");
      }
    }


    function stateChange() {
      if (asyncRequest.readyState == 4 && asyncRequest.status == 200) {
        document.getElementById("contentArea").innerHTML =
          asyncRequest.responseText;
      }
    }

    function clearPage() {
      document.getElementById("contentArea").innerHTML = "";
    }

    function init() {

      var z3 = document.getElementById("productLink");
      z3.addEventListener("mouseover", getAllProducts);

      var z4 = document.getElementById("productLink");
      z4.addEventListener("mouseout", clearPage);



    }
    document.addEventListener("DOMContentLoaded", init);

    //ajax in jQuery
    $(function() {
      $("#productDropDown").change(function() {
        $.ajax({
          url: "rtw-getCproducts.php?id=" + $("#productDropDown").val(),
          async: true,
          success: function(result) {
            $("#contentArea").html(result);
          }
        })
      })
    })
    //review text box size upgrade
    document.getElementById('textboxid').style.height = "200px";
    document.getElementById('textboxid').style.fontSize = "14pt";
  </script>
</head>

<body>
  <header class="main-header">
    <nav class="main-nav nav">
      <ul>
      <li><a href="st-store.php">Home</a></li>
      <li><a href="rtw-cLogin.php">Profile</a></li>
      <li><a href="st-orderhistory.php">Order History</a></li>
      <li><a href="st-index.php">Order Analysis</a></li>
      </ul>
    </nav>
    <h1 class="band-name band-name-large">KaffeBonor</h1>
  </header>
  <section class="container content-section">
    <h2 class="section-header">Profile</h2>
    <div>
      <?php
      session_start();

      if (isset($_SESSION['CID'])) {
        $CID = $_SESSION['CID'];
      } else {
        header("location:rtw-clogin");
      }

      require_once("db.php");
      if (isset($_POST["clogout"])) {
        echo '<script>alert("You Have been logged out")</script>';
        session_destroy();
        header("location:rtw-clogin");
      }
      if (isset($_POST["cdelete"])) {
        $sql = "delete from customers where CID = $CID";
        $result = $mydb->query($sql);
        if ($result == 1) {
          echo '<script>alert("Account Deleted!")</script>';
          session_destroy();

          header("location:rtw-clogin");
        } else {
          echo '<script>alert("Account could not be deleted, please try again")</script>';
        }
      }
      $PName = 0;
      $Rating = "";
      $ReviewText = "";
      $DateOfReview = "";

      $err = false;

      if (isset($_POST["submitreview"])) {
        if (isset($_POST["PName"])) $PID = $_POST["PName"];
        if (isset($_POST["star"])) $Rating = $_POST["star"];
        if (isset($_POST["ReviewText"])) $ReviewText = $_POST["ReviewText"];

        if (!empty($PID) && !empty($Rating) && !empty($ReviewText)) {
          require_once("db.php");
          date_default_timezone_set('America/New_York');
          $DateOfReview = Date('Y/m/d h:i:s', time());

          $sql = "insert into review(PID, CID, Rating, ReviewText, DateOfReview)
                        values($PID, $CID, '$Rating', '$ReviewText', '$DateOfReview')";
          $result = $mydb->query($sql);
          if ($result == 1) {

            echo '<script>alert("Review Added!")</script>';
          } else {
            echo '<script>alert("Error Adding Review")</script>';
          }
        } else {
          $err = true;
        }
      }


      $sql = "select * from customers where CID = '$CID'";
      $result = $mydb->query($sql);
      $row = mysqli_fetch_array($result);


      $LastName = $row['LastName'];
      $FirstName = $row['FirstName'];
      $Email = $row['Email'];
      $ShippingStreetAddress = $row['ShippingStreetAddress'];
      $ShippingCity = $row['ShippingCity'];
      $ShippingState = $row['ShippingState'];
      $ShippingZip = $row['ShippingZip'];


      ?>
      <label>First Name:
        <br />
        <input type="text" name="FirstName" value="<?php echo $FirstName; ?>" readonly />
      </label>
      <br />
      <label>Last Name:
        <br />
        <input type="text" name="LastName" value="<?php echo $LastName; ?>" readonly />
      </label>
      <br />
      <label>Email:
        <br />
        <input type="text" name="Email" value="<?php echo $Email; ?>" readonly />
      </label>
      <br />
      <label>Street Address:
        <br />
        <input type="text" name="ShippingStreetAddress" value="<?php echo $ShippingStreetAddress; ?>" readonly />
      </label>
      <br />
      <label>City:
        <br />
        <input type="text" name="ShippingCity" value="<?php echo $ShippingCity; ?>" readonly />
      </label>
      <br />
      <label>State:
        <br />
        <input type="text" name="ShippingState" value="<?php echo $ShippingState; ?>" readonly />
      </label>
      <br />
      <label>Zip Code:
        <br />
        <input name="ShippingZip" type="text" pattern="[0-9]*" value="<?php echo $ShippingZip; ?>" readonly>

      </label>
  </section>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" <br>
    <section class="container content-section"><br>
      <input type="submit" class="btn btn-primary" name="clogout" value="Log Out" />
    </section><br>
    <section class="container content-section">
      <a href="rtw-cmodify.php"><button class="btn btn-primary" type="button">Modify Account</button></a>
    </section><br>
    <section class="container content-section">
      <input type="submit" class="btn btn-primary" name="cdelete" value=" Delete Account" />
    </section><br>
    <section class="container content-section"><br>
      <h2 class="section-header">Review Products</h2>
      <div id="contentArea">&nbsp;</div>
      <a href="rtw-ratingd3.html"><button class="btn btn-primary" type="button">View Review Breakdown For Products</button></a>
      <br><br>
      <a id="productLink" class="btn btn-primary" href="">Hover To View All Purchased Products</a> <br />
      <br><br>
      <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" <label> Choose From Purchased Products: &nbsp;&nbsp;
        <br />
        <select name="PName" id="productDropDown" value="<?php echo $PID; ?>">
          <?php
          require_once("db.php");
          $sql = "select PID, PName from product
        where PName in 
         (select distinct PName from orderdetail
          inner join product
          on orderdetail.PID = product.PID
           inner join orders
           on orderdetail.OID  = orders.OID
           where CID = $CID);";
          $result = $mydb->query($sql);


          echo "<option value=''>'Select Purchased Product'</option>";
          while ($row = mysqli_fetch_array($result)) {
            echo "<option value='" . $row["PID"] . "'>" . $row["PName"] . "</option>";
          }
          if ($err && empty($PName)) {
            echo "<label class='errlabel'>Error: Please select a product name.</label>";
          }
          ?>
        </select>
        </label><br /> <br />
        <br><br>
        <label>Rating (5 Being Best):
          <br /> <br />


          <div class="rating">
            <input id="star5" name="star" type="radio" value="5" class="radio-btn hide" />
            <label for="star5">☆</label>
            <input id="star4" name="star" type="radio" value="4" class="radio-btn hide" />
            <label for="star4">☆</label>
            <input id="star3" name="star" type="radio" value="3" class="radio-btn hide" />
            <label for="star3">☆</label>
            <input id="star2" name="star" type="radio" value="2" class="radio-btn hide" />
            <label for="star2">☆</label>
            <input id="star1" name="star" type="radio" value="1" class="radio-btn hide" />
            <label for="star1">☆</label>
            <div class="clear"></div>
          </div>
          <?php
          if ($err && empty($Rating)) {
            echo "<label class='errlabel'>Error: Please enter a Rating.</label>";
          }
          ?>
        </label>
        <br />
        <label>Review:
          <br />
          <textarea name="ReviewText" cols="50" rows="10"></textarea>
          <br>
          <?php
          if ($err && empty($ReviewText)) {
            echo "<label class='errlabel'>Error: Please enter a review.</label>";
          }
          ?>
        </label>
        <br />
        <input type="submit" name="submitreview" value="Submit" />
    </section><br>
    <footer class="main-footer">
      <div class="container main-footer-container">
        <h3 class="band-name">KaffeBonor</h3>
        <ul class="nav footer-nav">
          <li>
            <a href="https://www.youtube.com" target="_blank">
              <img src="Images/YouTube Logo.png">
            </a>
          </li>
          <li>
            <a href="https://www.spotify.com" target="_blank">
              <img src="Images/Spotify Logo.png">
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com" target="_blank">
              <img src="Images/Facebook Logo.png">
            </a>
          </li>
          <li><a href="ras-employeeLogin.php">Employee Login</a></li>
        </ul>
      </div>
    </footer>
</body>

</html>