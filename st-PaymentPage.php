<?php
session_start();

if (isset($_SESSION['CID'])) {
  $CID = $_SESSION['CID'];
} else {
  header("location:st-login.php");
}
  require_once("db.php");
  $pid = 0;
  $pname = "";
  $sid = 0;
  $cid = 0;
  $uistock=0;
  $uprice = 0;
  $err = false;


  $sql = "select * from customers where CID = '$CID'";
  $result = $mydb->query($sql);
  $row = mysqli_fetch_array($result);


  $LastName = $row['LastName'];
  $FirstName = $row['FirstName'];
  $Email = $row['Email'];
  $ShippingStreetAddress = $row['ShippingStreetAddress'];
  $ShippingCity = $row['ShippingCity'];
  $ShippingState = $row['ShippingState'];

  if (isset($_POST["submit"])) {
      if(isset($_POST["cardnumber"])) $card=$_POST["cardnumber"];
      if(isset($_POST["billingaddress"])) $baddress=$_POST["billingaddress"];
      if(isset($_POST["pin"])) $pin=$_POST["pin"];

      if($card>=0 && !empty($baddress) && $pin>0) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: st-SuccessfullPayment.php");
      } else {
        $err = true;
      }
  }

 ?>
<!DOCTYPE html>
<html>
    <head>
        <title>KaffeBonor | Payment Page</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
        <section class="container content-section">
            <h2 class="section-header">Please fill in your payment info:</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
              <label>Email Address: </label>
              <input type="text" name="email" value="<?php echo $Email; ?>" disabled/>
              <br />

              <label>Full Name:</label>
              <input name="name" type="text" value = "<?php echo $FirstName." ".$LastName; ?>" disabled/>
              <br />

              <label for="shippingaddress">Shipping address:</label>
              <input type = "text" name="shippingaddress" value ="<?php echo $ShippingStreetAddress; ?>" disabled/>
              <br />

              <label for="cardnumber">Card Number:</label>
              <input type = "text" name="cardnumber"/>
              <?php
                if ($err && empty($card)) {
                  echo "<label class='errlabel'>Error: Please fill in your card number.</label>";
                }
               ?>
              <br />
              <label for="pin">3-Pin:</label>
              <input type = "text" name="pin"/>
              <?php
                if ($err && empty($pin)) {
                  echo "<label class='errlabel'>Error: Please fill in your 3-Pin (located on the back of the card).</label>";
                }
               ?>
              <br />
              <label for="billingaddress">Billing address:</label>
              <input type = "text" name="billingaddress"/>
              <?php
                if ($err && empty($baddress)) {
                  echo "<label class='errlabel'>Error: Please fill in your billing address.</label>";
                }
               ?>
              <br />

              <br />
              <br />
              <input type="submit" class ="btn btn-primary btn-submitpayment" name="submit" value="Complete Payment" />
              <br />
            </form>

        </section>
        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
