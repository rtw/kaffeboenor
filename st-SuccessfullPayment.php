<!DOCTYPE html>

<html>
    <head>
        <title>KaffeBonor | Successfull Payment</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>

        <section class="container content-section">
            <h2 class="section-header">Thank You for your purchase!</h2>
            <p class="ThankYou-Text">Thank you so much for doing business with us. Listed below is what you ordered:</p>

            <?php
            if(isset($_POST["cancel"])){
              session_start();
              if (isset($_SESSION['CID'])) {
                $CID = $_SESSION['CID'];
              }
              require_once("db.php");
              $DateOfPurchase = "";
              date_default_timezone_set('America/New_York');
              $DateOfPurchase = Date('yy-m-d', time());
              $sql = "DELETE FROM bit4444group37.orders WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
              $result = $mydb->query($sql);
              unset($_SESSION['my_array']);
              header("location:st-cancelpage.php");

            } elseif (isset($_POST["backbutton"])){
              session_start();
              unset($_SESSION['my_array']);
              header("location:st-store.php");

            } elseif (isset($_POST["modify"])) {
              header("location:st-modify.php");
            }else {

            require_once("db.php");
            session_start();
            if (isset($_SESSION['CID'])) {
              $CID = $_SESSION['CID'];
            }
            $keys = array_keys($_SESSION['my_array']);
            echo "<table class = 'cartinfo' class ='ThankYou-Text'>";
            echo "<thead style='background-color:black'><tr><th><font color = white>Product Name</font></th>
            <th><font color = white>Price</font></th>
            <th><font color = white>Quantity</font></th>";
            $DateOfPurchase = "";
            $TimeOfPurchase = "";
            $totalprice = 0;
            $totalquantity = 0;
            $TotalTax = 0;
            $TotalShippingCosts = 0;
            $TotalAfterTaxCosts = 0;

            date_default_timezone_set('America/New_York');
            $DateOfPurchase = Date('Y-m-d h:i:s', time());
            $TimeOfPurchase = Date('yy-m-d', time());

            for($i = 0; $i <count($_SESSION['my_array']); $i++){
              global $totalprice, $totalquantity;
               echo "<tr>";
              foreach($_SESSION['my_array'][$i] as $key =>$value){
                echo "<td style='background-color:lightgrey'><font color = white>".$value."</font></td>";

              }

              $productName = $_SESSION['my_array'][$i][0];
              $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity -".$_SESSION['my_array'][$i][2].
              " WHERE PName = '$productName' AND InStockQuantity > 0";
              $result = $mydb->query($sql);
              $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][$i][1]) * $_SESSION['my_array'][$i][2]);
              $totalquantity = $totalquantity + $_SESSION['my_array'][$i][2];
              echo "<tr>";
              echo "<br />";
            }
            echo "<table/>";

            $TotalTax = $totalprice *0.053;
            $TotalShippingCosts = $totalquantity * 0.5;
            $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

            echo "<p class='ThankYou-Text'>You ordered a total quantity of: ".$totalquantity.".</p>";
            echo "<p class='ThankYou-Text'>For a total price of (Tax Included): $".round($TotalAfterTaxCosts,2).".</p>";
            echo "<p class='ThankYou-Text'>Date of purchase: ".$TimeOfPurchase.".</p>";

            $sql = "INSERT INTO bit4444group37.orders (CID, DateOfPurchase, TotalPreTaxCosts, TotalTax, TotalShippingCosts,
            totalCosts, TotalQuantity, PaymentStatus, ShipmentStatus) VALUES
            ($CID, '$DateOfPurchase', $totalprice, $TotalTax, $TotalShippingCosts, $TotalAfterTaxCosts, $totalquantity, 'Complete', 'Order Recieved')";
            $result = $mydb->query($sql);

            }


             ?>
             <form method="post">
             <button class="btn btn-primary btn-cancel" name = "cancel" type="submit" action="<?php echo $_SERVER['PHP_SELF'] ?>">Cancel Order</button>
             <button class="btn btn-primary btn-modify" name = "modify" type="submit" action="<?php echo $_SERVER['PHP_SELF'] ?>" >Modify my order</button>
             <button class="btn btn-primary btn-homepage" name = "backbutton" type="submit" action="<?php echo $_SERVER['PHP_SELF'] ?>" >Back to products page</button>
             </form>
        </section>
        <footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
