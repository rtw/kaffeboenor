<!DOCTYPE html>
<?php
  session_start();
if(isset($_POST["logout"])){
  session_destroy();
  header("location:st-store.php");
}
 ?>
<html>
    <head>
        <title>KaffeBonor | Cancel Page</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>

        <section class="container content-section">
            <h2 class="section-header">Your order was successfully cancelled.</h2>
            <form method="post">
             <button class="btn btn-primary btn-purchase" type="submit" name ="logout">Logout</button>
           </form>
        </section>
        <footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>

            </div>
        </footer>
    </body>
</html>
