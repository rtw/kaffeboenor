<?php
  session_start();
  if (isset($_SESSION['CID'])) {
    $CID = $_SESSION['CID'];
  } else {
    echo '<script>alert("You need to log in first.")</script>';
    header("location:st-store.php");
  }
  require_once("db.php");
 ?>
<html>
<head>
  <title>KaffeBonor | Analyze</title>
  <meta name="description" content="This is the description">
  <link rel="stylesheet" href="st-styles.css" />
  <script src="jquery-3.1.1.min.js"></script>
</head>
<body>
  <header class="main-header">
      <nav class="main-nav nav">
          <ul>
          <li><a href="st-store.php">Home</a></li>
          <li><a href="rtw-cLogin.php">Profile</a></li>
          <li><a href="st-orderhistory.php">Order History</a></li>
          <li><a href="st-index.php">Order Analysis</a></li>
          </ul>
      </nav>
      <h1 class="band-name band-name-large">KaffeBonor</h1>
  </header>
  <section class="container content-section">
  <h2 class="section-header">Select your order id you want to analyze:</h2>
  <form class = "center" method='get' action='st-showProducts.php'>
    <select name='OID'>
    <?php
        require_once("db.php");
        $sql = "SELECT OID, CID FROM bit4444group37.orders WHERE CID = $CID ORDER BY OID";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["OID"]."'>".$row["OID"]."</option>";
        }
      ?>
    </select>
    <br>
    <input class ="btn btn-primary" name='submit' type='submit' />

  </form>
</section>
<footer class="main-footer">
    <div class="container main-footer-container">
        <h3 class="band-name">KaffeBonor</h3>
        <ul class="nav footer-nav">
            <li>
                <a href="https://www.youtube.com" target="_blank">
                    <img src="Images/YouTube Logo.png">
                </a>
            </li>
            <li>
                <a href="https://www.spotify.com" target="_blank">
                    <img src="Images/Spotify Logo.png">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com" target="_blank">
                    <img src="Images/Facebook Logo.png">
                </a>
            </li>
            <li><a href="ras-employeeLogin.php">Employee Login</a></li>
        </ul>
    </div>
</footer>

</body>
</html>
