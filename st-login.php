<!DOCTYPE html>
<html>
    <head>
        <title>KaffeBonor | Login</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="st-storejs.php" async></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
        <section class="container content-section">
            <h2 class="section-header">Customer Login</h2>
            <?php
            session_start();
            $CID = 0;
            $Email = "";
            $CPassword = "";
            $error = false;
            $loginOK = null;

            if (isset($_POST["submit"])) {
              if(isset($_POST["Email"])) $Email=$_POST["Email"];
              if(isset($_POST["CPassword"])) $CPassword=$_POST["CPassword"];
              if(empty($Email) || empty($CPassword)) {
                $error=true;
              }


              if (!$error) {
                require_once("db.php");
                $sql = "select CID, Email, CPassword from customers where Email = '$Email' and CPassword = '$CPassword'";
                $result = $mydb->query($sql);

                $row=mysqli_fetch_array($result);

                if ($row){
                  if (strcmp($Email, $row["Email"]) == 0 && strcmp($CPassword, $row["CPassword"]) == 0) {
                    $CID = $row["CID"];

                    $loginOK = true;
                  } else {
                    $loginOK = false;
                  }

                  if($loginOK) {
                    if (isset($_SESSION['my_array'])) {
                      $_SESSION['CID'] = $CID;
                      header("location:st-PaymentPage.php");
                    }else{
                      header("location:st-store.php");
                      $_SESSION['CID'] = $CID;
                    }

                  }else{
                    alert("Login failed");
                  }

                }
              }

            }
            ?>
              <form class ="center" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                <label for ="Email">Email:</label>
                  <input type="text" name="Email"/>
                  <?php
                  if($error && empty($Email)){
                      echo "<label class='errlabel'>Error: Please enter your Email.</label>";
                    }
                  ?>
                <br />
                <label for ="CPassword">Password:</label>
                  <input type="password" name="CPassword"/>
                  <?php
                  if($error && empty($CPassword)){
                      echo "<label class='errlabel'>Error: Please enter your Password.</label>";
                    }
                  ?>
                <br />
                <input class ="btn btn-primary" type="submit" name="submit" value="Login" />
            </body>

        </section>

        <section class="container content-section">
            <a href="st-store.php"><button class="btn btn-primary btn-back" type="button">Back to Products</button></a>
        </section>
        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
