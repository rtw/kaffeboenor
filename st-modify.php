<?php
session_start();

if (isset($_SESSION['CID'])) {
  $CID = $_SESSION['CID'];
} else {
  header("location:st-login.php");
}

if (isset($_POST["modifydone"])){
  unset($_SESSION['my_array']);
  header("location:st-store.php");
}


 ?>
<!DOCTYPE html>
<html>
    <head>
        <title>KaffeBonor | Modify</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
        <section class="container content-section">
            <h2 class="section-header">What do you want to modify about your order?:</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
              <?php
              require_once("db.php");
              if (isset($_SESSION['CID'])) {
                $CID = $_SESSION['CID'];
              }
              $keys = array_keys($_SESSION['my_array']);
              echo "<table class = 'cartinfo' class ='ThankYou-Text'>";
              echo "<thead style='background-color:black'><tr><th><font color = white>Product Name</font></th>
              <th><font color = white>Price</font></th>
              <th><font color = white>Quantity</font></th>";
              $item = 0;
              $DateOfPurchase = "";
              $TimeOfPurchase = "";
              $totalprice = 0;
              $totalquantity = 0;
              $TotalTax = 0;
              $TotalShippingCosts = 0;
              $TotalAfterTaxCosts = 0;

              date_default_timezone_set('America/New_York');
              $DateOfPurchase = Date('Y-m-d h:i:s', time());
              $TimeOfPurchase = Date('yy-m-d', time());

              for($i = 0; $i <count($_SESSION['my_array']); $i++){
                global $totalprice, $totalquantity;
                 echo "<tr>";
                foreach($_SESSION['my_array'][$i] as $key =>$value){
                  echo "<td style='background-color:lightgrey'><font color = white>".$value."</font></td>";

                }
              }
              echo "<table/>";
              echo "<br />";
              echo "<form method='post'>";
              for($i = 0; $i <count($_SESSION['my_array']); $i++){
                  $item = $i + 1;
                  echo "<br />";
                  echo "<button class='btn btn-primary center' name ='".$i."' type='submit' action='".$_SERVER['PHP_SELF']."'>Remove item #".$item."</button>";
                  echo "<input class ='modifybtn' type ='text' name ='".$i."' value ='".$_SESSION['my_array'][$i][0]."'>";
                  echo "<br />";
              }
              echo "</form>";
              //I know this code doesnt grab everything dynamically, but I couldn't figure out another way of doing this.
              if (isset($_POST["0"])){
                require_once("db.php");
                $productName = $_SESSION['my_array'][0][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][0][1]) * $_SESSION['my_array'][0][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][0][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][0][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";


              } elseif (isset($_POST["1"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][1][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][1][1]) * $_SESSION['my_array'][1][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][0][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][1][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][1][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["2"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][2][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][2][1]) * $_SESSION['my_array'][2][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][0][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][2][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][2][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["3"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][3][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][3][1]) * $_SESSION['my_array'][3][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][0][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][3][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][3][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["4"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][4][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][4][1]) * $_SESSION['my_array'][4][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][0][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][4][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][4][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["5"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][5][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][5][1]) * $_SESSION['my_array'][5][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][5][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][5][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][5][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["6"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][6][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][6][1]) * $_SESSION['my_array'][6][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][6][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][6][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][6][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              } elseif (isset($_POST["7"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][7][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][7][1]) * $_SESSION['my_array'][7][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][7][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][7][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][7][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              }elseif (isset($_POST["8"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][8][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][8][1]) * $_SESSION['my_array'][8][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][8][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][8][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][8][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";

              }elseif (isset($_POST["9"])) {
                require_once("db.php");
                $productName = $_SESSION['my_array'][9][0];
                $totalprice = $totalprice + (str_replace('$','',$_SESSION['my_array'][9][1]) * $_SESSION['my_array'][9][2]);
                $totalquantity = $totalquantity + $_SESSION['my_array'][9][2];
                $TotalTax = $totalprice *0.053;
                $TotalShippingCosts = $totalquantity * 0.5;
                $TotalAfterTaxCosts = $totalprice + $TotalTax + $TotalShippingCosts;

                $productName = $_SESSION['my_array'][9][0];
                $sql= "UPDATE bit4444group37.product SET InStockQuantity = InStockQuantity +".$_SESSION['my_array'][9][2].
                " WHERE PName = '$productName' AND InStockQuantity > 0";
                $result = $mydb->query($sql);

                $sql="UPDATE bit4444group37.orders SET TotalPreTaxCosts = TotalPreTaxCosts - $totalprice, TotalTax = TotalTax - ".round($TotalTax,2)."
                , TotalShippingCosts = TotalShippingCosts -$TotalShippingCosts, TotalCosts = TotalCosts -".round($TotalAfterTaxCosts,2).",
                TotalQuantity = TotalQuantity - $totalquantity WHERE CID = $CID ORDER BY OID DESC LIMIT 1";
                $result = $mydb->query($sql);
                echo "<p>The item was removed successfully!</p>";
              }



               ?>
              <input type="submit" class ="btn btn-primary btn-submitpayment" name="modifydone" value="Submit Modifcation" />
              <br />
            </form>

        </section>
        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
