<!DOCTYPE html>
<html>
    <head>
        <title>KaffeBonor | Order history</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>  
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
        <section class="container content-section">
            <h2 class="section-header">Here are your recent orders:</h2>
            <?php
            session_start();
            if (isset($_SESSION['CID'])) {
              $CID = $_SESSION['CID'];
            } else {
              echo '<script>alert("You need to log in first.")</script>';
              header("location:st-store.php");
            }
              require_once("db.php");
              $sql = "SELECT * FROM bit4444group37.orders WHERE CID = $CID";
              $result = $mydb->query($sql);
              $row = mysqli_fetch_array($result);

              echo "<table border = 1>";
              echo "<thead style='background-color:black'><tr><th><font color = white>Order ID</font></th>
              <th><font color = white>Customer ID</font></th>
              <th><font color = white>Date of purchase</font></th>
              <th><font color = white>Total Pre-Tax Price</font></th>
              <th><font color = white>Total Tax</font></th>
              <th><font color = white>Total Shipping Cost</font></th>
              <th><font color = white>Total Cost</font></th>
              <th><font color = white>Total Quantity</font></th>
              <th><font color = white>Payment Status</font></th>
              <th><font color = white>Shipment Status</font></th></tr></thead>";
              while($row=mysqli_fetch_array($result)){
                echo "<tr>";
                echo "<td style='background-color:lightgrey'><font color = white>".$row["OID"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>".$row["CID"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>".$row["DateOfPurchase"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>$".$row["TotalPreTaxCosts"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>$".$row["TotalTax"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>$".$row["TotalShippingCosts"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>$".$row["TotalCosts"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>".$row["TotalQuantity"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>".$row["PaymentStatus"]."</font></td>"
                ."<td style='background-color:lightgrey'><font color = white>".$row["ShipmentStatus"]."</font></td>";
                echo "</tr>";
                }
              echo "<table/>";
             ?>

        </section>
        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
