<!DOCTYPE html>
<?php
session_start();
require_once("db.php");
require_once("st-storejs.php");
if(isset($_POST["logouthome"])){
    session_destroy();
    echo "<script>window.location='st-login.php'</script>";
    
}
?>
<html>
    <head>
        <title>KaffeBonor | Product Page</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="st-styles.css" />
        <script src="st-storejs.php" async></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                 <li><a href="rtw-cLogin.php">Profile</a></li>
                 <li><a href="st-orderhistory.php">Order History</a></li>
                 <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
            <div class="content-section">
            <input id="searchbar" onkeyup="search()" type="text"
            name="search" placeholder="Search for products">
            <?php
            echo "<ul id ='list' style ='display:none;'>";
            $sql= "SELECT PID, PName, PPrice, PDescription FROM bit4444group37.product";
            $result = $mydb->query($sql);
              while($row=mysqli_fetch_array($result)){
              echo "<li  class = 'searchitem'>".$row["PName"]."</li>";
            }
            echo "</ul>";
            ?>
            </div>
        </header>
        <?php 
            
        if(isset($_SESSION['CID'])){
            echo "<form method='post'>";
            echo "<button id ='paymentbutton' class='btn btn-primary btn-homepage' name = 'logouthome' type='submit'>Logout</button>";
            echo "</form>";
        } else{
            echo "";
        }
        ?>
    
        <section class="container content-section container-fluid">
            <h2 class="section-header">Coffee Beans</h2>
            <?php
            echo "<div class='shop-items'>";
            $sql= "SELECT PID, PName, PPrice, PDescription FROM bit4444group37.product";
            $result = $mydb->query($sql);

            while($row=mysqli_fetch_array($result)){
              echo "
                <div class='shop-item'>
                    <span class='shop-item-title'>".$row["PName"]."</span>
                    <img class='shop-item-image' src='Images/beans.jpeg'>
                    <div class='shop-item-details'>
                        <span class='shop-item-price'>$".$row["PPrice"]."</span>
                        <button class='btn btn-primary details-button' type='button'>DETAILS</button>
                        <p class='description' id = 'description'>".$row['PDescription']."</p>
                        <button class='btn btn-primary shop-item-button' type='button'>ADD TO CART</button>
                    </div>
                </div>";
              }
              echo "</div>";
             ?>


        </section>
        <section class="container content-section">
            <h2 class="section-header">CART</h2>
            <div class="cart-row">
                <span class="cart-item cart-header cart-column">ITEM</span>
                <span class="cart-price cart-header cart-column">PRICE</span>
                <span class="cart-quantity cart-header cart-column">QUANTITY</span>
            </div>
            <div class="cart-items">

            </div>
            <div class="cart-total">
                <strong class="cart-total-title">Total</strong>
                <span class="cart-total-price">$0</span>
            </div>
            
            <button id ="paymentbutton" class="btn btn-primary btn-purchase" name = "submit" type="button">CONTINUE TO PAYMENT</button>
            
        </section>
        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>
