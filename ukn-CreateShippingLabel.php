<?php
  $fname = "";
  $lname = "";
  $street = "";
  $city = "";
  $state = "";
  $zipcode = "";
  $err = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["firstname"])) $fname=$_POST["firstname"];
      if(isset($_POST["lastname"])) $lname=$_POST["lastname"];
      if(isset($_POST["streetaddress"])) $street=$_POST["streetaddress"];
      if(isset($_POST["city"])) $city=$_POST["city"];
      if(isset($_POST["state"])) $state=$_POST["state"];
      if(isset($_POST["zipcode"])) $zipcode=$_POST["zipcode"];

      if(!empty($fname) && !empty($lname) && !empty($street) && !empty($city) && !empty($state) && !empty($zipcode)) {
        header("HTTP/1.1 307 Temprary Redirect");
        header("Location: ukn-ShippingLabelCreated.php");
      } else {
        $err = true;
      }
  }
 ?>

<!doctype html>
<html>
<head>
  <title>Create Shipping Label</title>
  <link rel="stylesheet" href="st-styles.css" />
  <script src="jquery-3.1.1.min.js"></script>
  <style>
    .errlabel {color:red;}

  </style>
</head>

<body>
<header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
<h1>Create Shipping Label</h1>
<label><strong>Please enter customer details below:</strong></label>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    
    <p>
    <label>First Name:</label>
    <input name="firstname" type="text" value="<?php echo $fname; ?>"/>
    <?php
      if ($err && empty($fname)) {
        echo "<label class='errlabel'>Please enter the customer's first name.</label>";
      }
    ?>
    <br/>
    </p>

    <label>Last Name:</label>
    <input name="lastname" type="text" value="<?php echo $lname; ?>"/>
    <?php
      if ($err && empty($lname)) {
        echo "<label class='errlabel'>Please enter the customer's last name.</label>";
      }
    ?>
    <br/>

    <p>
    <label>Street Address:</label>
    <input name="streetaddress" type="text" value="<?php echo $street; ?>"/>
    <?php
      if ($err && empty($street)) {
        echo "<label class='errlabel'>Please enter customer's street address.</label>";
      }
    ?>
    <br />
    </p>

    <label>City:</label>
    <input name="city" type="text" value="<?php echo $city; ?>"/>
    <?php
      if ($err && empty($city)) {
        echo "<label class='errlabel'>Please enter the customer's city.</label>";
      }
    ?>
    <br />
    
    <p>
    <label>State:</label>
    <select name="state" value="<?php echo $state; ?>">
        <option value=""></option>
        <option <?php if($state=='AL'){echo "selected";}?> value="AL">AL</option>
        <option <?php if($state=='AK'){echo "selected";}?> value="AK">AK</option>
        <option <?php if($state=='AZ'){echo "selected";}?> value="AZ">AZ</option>
        <option <?php if($state=='AR'){echo "selected";}?> value="AR">AR</option>
        <option <?php if($state=='CA'){echo "selected";}?> value="CA">CA</option>
        <option <?php if($state=='CO'){echo "selected";}?> value="CO">CO</option>
        <option <?php if($state=='CT'){echo "selected";}?> value="CT">CT</option>
        <option <?php if($state=='DE'){echo "selected";}?> value="DE">DE</option>
        <option <?php if($state=='FL'){echo "selected";}?> value="FL">FL</option>
        <option <?php if($state=='GA'){echo "selected";}?> value="GA">GA</option>
        <option <?php if($state=='HI'){echo "selected";}?> value="HI">HI</option>
        <option <?php if($state=='ID'){echo "selected";}?> value="ID">ID</option>
        <option <?php if($state=='IL'){echo "selected";}?> value="IL">IL</option>
        <option <?php if($state=='IN'){echo "selected";}?> value="IN">IN</option>
        <option <?php if($state=='IA'){echo "selected";}?> value="IA">IA</option>
        <option <?php if($state=='KS'){echo "selected";}?> value="KS">KS</option>
        <option <?php if($state=='KY'){echo "selected";}?> value="KY">KY</option>
        <option <?php if($state=='LA'){echo "selected";}?> value="LA">LA</option>
        <option <?php if($state=='ME'){echo "selected";}?> value="ME">ME</option>
        <option <?php if($state=='MD'){echo "selected";}?> value="MD">MD</option>
        <option <?php if($state=='MA'){echo "selected";}?> value="MA">MA</option>
        <option <?php if($state=='MI'){echo "selected";}?> value="MI">MI</option>
        <option <?php if($state=='MN'){echo "selected";}?> value="MN">MN</option>
        <option <?php if($state=='MS'){echo "selected";}?> value="MS">MS</option>
        <option <?php if($state=='MO'){echo "selected";}?> value="MO">MO</option>
        <option <?php if($state=='MT'){echo "selected";}?> value="MT">MT</option>
        <option <?php if($state=='NE'){echo "selected";}?> value="NE">NE</option>
        <option <?php if($state=='NV'){echo "selected";}?> value="NV">NV</option>
        <option <?php if($state=='NH'){echo "selected";}?> value="NH">NH</option>
        <option <?php if($state=='NJ'){echo "selected";}?> value="NJ">NJ</option>
        <option <?php if($state=='NM'){echo "selected";}?> value="NM">NM</option>
        <option <?php if($state=='NY'){echo "selected";}?> value="NY">NY</option>
        <option <?php if($state=='NC'){echo "selected";}?> value="NC">NC</option>
        <option <?php if($state=='ND'){echo "selected";}?> value="ND">ND</option>
        <option <?php if($state=='OH'){echo "selected";}?> value="OH">OH</option>
        <option <?php if($state=='OK'){echo "selected";}?> value="OK">OK</option>
        <option <?php if($state=='OR'){echo "selected";}?> value="OR">OR</option>
        <option <?php if($state=='PA'){echo "selected";}?> value="PA">PA</option>
        <option <?php if($state=='RI'){echo "selected";}?> value="RI">RI</option>
        <option <?php if($state=='SC'){echo "selected";}?> value="SC">SC</option>
        <option <?php if($state=='SD'){echo "selected";}?> value="SD">SD</option>
        <option <?php if($state=='TN'){echo "selected";}?> value="TN">TN</option>
        <option <?php if($state=='TX'){echo "selected";}?> value="TX">TX</option>
        <option <?php if($state=='UT'){echo "selected";}?> value="UT">UT</option>
        <option <?php if($state=='VT'){echo "selected";}?> value="VT">VT</option>
        <option <?php if($state=='VA'){echo "selected";}?> value="VA">VA</option>
        <option <?php if($state=='WA'){echo "selected";}?> value="WA">WA</option>
        <option <?php if($state=='WV'){echo "selected";}?> value="WV">WV</option>
        <option <?php if($state=='WI'){echo "selected";}?> value="WI">WI</option>
        <option <?php if($state=='WY'){echo "selected";}?> value="WY">WY</option>
    </select>
    <?php
      if ($err && empty($state)) {
        echo "<label class='errlabel'>Please select the customer's state.</label>";
      }
    ?>
    <br/>
    </p>
    
    <p>
    <label>Zip Code:</label>
    <input name="zipcode" type="text" value="<?php echo $zipcode; ?>"/>
    <?php
      if ($err && empty($zipcode)) {
        echo "<label class='errlabel'>Please enter the customer's zipcode.</label>";
      }
    ?>
    <br/>
    </p>

    <input type="submit" name="submit" value="Submit" />
    <br />
  </form>
<p>
    <a href="ukn-ShippingHome.php"><button class="btn btn-primary" type="button">Return to Shipping and Orders</button></a></br>
</p>
<footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
</body>
</html>
