<!doctype html>
<head>
  <title>Kaffe Boenor Employee Home</title>
  
    <meta charset="utf-8" />                     
    <link rel="stylesheet" href="styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    
</head>
<body>
<header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
</header>
 
<div class="container-fluid">
<br>
<p><h2>Shipping and Orders Management</h2></p>

<p>
<a href="ras-kn-employeeMain.php"><button class="btn btn-primary" type="button">Employee Home</button></a></br>
</p>
<p> 
    <a href="ukn-CreateShippingLabel.php"><button class="btn btn-primary" type="button">Create Shipping Label</button></a></br>
</p>
<p>
    <a href="ukn-updateShipmentStatus.php"><button class="btn btn-primary" type="button">Update Shipment Status</button></a></br>
</p>
<p>
    <a href="ukn-searchOrders.php"><button class="btn btn-primary" type="button">Search Orders</button></a></br>
</p>

</div>
</body>
