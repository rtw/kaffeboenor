<!doctype html>
<html>
<head>
<link rel="stylesheet" href="st-styles.css" />
<script src="jquery-3.1.1.min.js"></script>
<style>
    div.a {
      width: 500px;
      border: 5px solid black;
      padding: 15px;
      margin: 15px;
    }
    p.a {
      text-align: center;
      font-size: 40px;
      font-weight: bold;
    }
    p.b {
      text-align: center;
      font-weight: bold;
      font-size: 20px;
    }
    p.c {
    
    }
</style>
  <title>Shipping Label Created</title>
</head>
<body>
<header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
<header class="main-header">
            <nav class="main-nav nav">
                <ul>
                  <li><a href="home.html">Home</a></li>
                  <li><a href="st-store.php">Products</a></li>
                  <li><a href="st-about.html">About</a></li>
                  <li><a href="st-login.php">Login</a></li>
                  <li><a href="st-orderhistory.php">Order History</a></li>
                  <li><a href="st-index.php">Order History</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
  <?php
    $fname = "";
    $lname = "";
    $street = "";
    $city = "";
    $state = "";
    $zipcode = "";
  
        if(isset($_POST["firstname"])) $fname=$_POST["firstname"];
        if(isset($_POST["lastname"])) $lname=$_POST["lastname"];
        if(isset($_POST["streetaddress"])) $street=$_POST["streetaddress"];
        if(isset($_POST["city"])) $city=$_POST["city"];
        if(isset($_POST["state"])) $state=$_POST["state"];
        if(isset($_POST["zipcode"])) $zipcode=$_POST["zipcode"];
  ?>

  <!-- Shipping Label Details/Format -->
  
  
  <div class="a">
    Kaffebönor</br>
    420 Main Street</br>
    Blacksburg, VA 24061</br>
    United States</br>
  
    <p class="a">USPS PRIORITY MAIL</p></br>
    SHIP TO:
    <p class="b">
      <?php echo $fname;?> <?php echo $lname;?></br>
      <?php echo $street;?></br> 
      <?php echo $city;?>, <?php echo $state;?></br>
      <?php echo $zipcode;?></br>
    </p>
   
  Tracking #</br>
  <p class="c"> 
  <?php 
    echo bin2hex(random_bytes(10));
  ?>
  </p>
</div>
<p>
<button class='btn1' onclick="print()">Print Shipping Label</button></br>
<script>
   function print() {
      alert("Shipping Label Printed");
      window.location.href = 'ukn-printShippingLabel.php';
   }
</script>
</p>
<p>
<button class='btn1' onclick="create()">Create Another Shipping Label</button></br>
<script>
   function create() {
      window.location.href = 'ukn-createShippingLabel.php';
   }
</script>
</p>
<p>
<button class='btn' onclick="cancel()" >Cancel Shipping Label</button>
<script>
   function cancel() {
      alert("Shipping Label Cancelled");
      window.location.href = 'ukn-cancelShippingLabel.php';
   }
</script>
</p>
<p>
    <a href="ukn-ShippingHome.php"><button class="btn btn-primary" type="button">Return to Shipping and Orders</button></a></br>
</p>
</p>
<footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
</body>
</html>
