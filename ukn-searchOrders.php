<!doctype html>
<html>
<head>
  <title>Search Orders</title>
  <link rel="stylesheet" href="styles.css" />
  <script src="jquery-3.1.1.min.js"></script>
  <script>
		var asyncRequest;

    function getAllOrders() {
      var url = "ukn-displayOrders.php";
        try {
          asyncRequest = new XMLHttpRequest();

          asyncRequest.onreadystatechange=stateChange;
          asyncRequest.open('GET',url,true);
          asyncRequest.send();
        }
          catch (exception) {alert("Request failed");}
    }
    
		function stateChange() {
			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				document.getElementById("contentArea").innerHTML=
					asyncRequest.responseText;
			}
		}

    function clearPage(){
      document.getElementById("contentArea").innerHTML = "";
    }

    function init(){

      var z3 = document.getElementById("productLink");
      z3.addEventListener("mouseover", getAllOrders);

      var z4 = document.getElementById("productLink");
      z4.addEventListener("mouseout", clearPage);    

    }
    document.addEventListener("DOMContentLoaded", init);

    $(function(){
      $("#orderDropDown").change(function(){
        $.ajax({
          url:"ukn-displayOrders.php?oid=" + $("#orderDropDown").val(),
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      })
      $("#updatebutton").click(function(){
       $.ajax({
         
          url:"ras-updateproductdatabase.php?OID="+ $("#orderDropDown").val() ,
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      }) 
    })        
	</script>
</head>

<body>
<header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>
<h1>Search Orders</h1>
<p>
  <a href="ukn-ShippingHome.php"><button class="btn btn-primary" type="button">Return to Shipping and Orders</button></a></br>
</p>
<p>
  <a href="ukn-searchOrderDetails.php"><button class="btn btn-primary" type="button">Search Order Details</button></a></br>
</p>
<br>
  <a id="productLink"><button class="btn-btn-primary" type="button">Display All Orders</button></a> <br />
  <label> Search by Order ID: &nbsp;&nbsp;
    <select name="OID" id="orderDropDown">
      <?php
        require_once("db.php");
        $sql = "select * from orders";
        $result = $mydb->query($sql);

        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["OID"]."'>".$row["OID"]."</option>";          
        }      
      ?>   
    </select>
  </label><br/>

  <div id="contentArea">&nbsp;</div>

 </label>

 <footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>