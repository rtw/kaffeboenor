<?php
session_start();
    $SID = 0;
    if(isset($_GET["SID"])) $SID=$_GET["SID"];
    
    require_once("db.php");
    $sql = "select * from shipping where SID = '$SID'";
    $result = $mydb->query($sql);
    $row = mysqli_fetch_array($result);


    $SID = $row['SID'];
    $CurrentStatus = $row['CurrentStatus'];
   

    if (isset($_POST["update"])) {
        if (isset($_POST["SID"])) $SID = $_POST["SID"];
        if (isset($_POST["CurrentStatus"])) $CurrentStatus = $_POST["CurrentStatus"];
          
            $sql = "update shipping set SID = '$SID', CurrentStatus = '$CurrentStatus' where SID = '$SID'";             
            $result = $mydb->query($sql);
            
            if ($result == 1) {
                function function_alert($Success) { 
                    echo "<script>alert('$Success');</script>"; 
                } 
                function_alert("Update Successful"); 
            } else {
                function function_alert($NoSuccess) { 
                    echo "<script>alert('$NoSuccess');</script>"; 
                } 
                function_alert("Update Unsuccessful"); 
            }
    }
?>

<html>
    <head>
        <title>Update Shipment Status</title>
    </head>
    <link rel="stylesheet" href="st-styles.css" />
    <script src="jquery-3.1.1.min.js"></script>
    <body>
    <header class="main-header">
            <nav class="main-nav nav">
                <ul>
                <li><a href="st-store.php">Home</a></li>
                <li><a href="rtw-cLogin.php">Profile</a></li>
                <li><a href="st-orderhistory.php">Order History</a></li>
                <li><a href="st-index.php">Order Analysis</a></li>
                </ul>
            </nav>
            <h1 class="band-name band-name-large">KaffeBonor</h1>
        </header>

    <h1>Update Shipment Status<h1>
        <form action="" method="POST">
            <input type="text" name="SID" placeholder="Enter Shipping ID"/><br/>
            <Select name="CurrentStatus" placeholder="Update Shipment Status">
                <option></option>
                <option>Order Received</option>
                <option>In Transit</option>
                <option>Delivered</option>
                <option>Lost</option>                        
            </select><br/>

            <input type="submit" name="update" value="Update Shipment Status"/>
        </form>

        <p>
            <a href="ukn-ShippingHome.php"><button class="btn btn-primary" type="button">Return to Shipping and Orders</button></a></br>
        </p>
    <footer class="main-footer">
          <input type="hidden" name="" value="">
            <div class="container main-footer-container">
                <h3 class="band-name">KaffeBonor</h3>
                <ul class="nav footer-nav">
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="Images/YouTube Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.spotify.com" target="_blank">
                            <img src="Images/Spotify Logo.png">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                    <li><a href="ras-employeeLogin.php">Employee Login</a></li>
                </ul>
            </div>
        </footer>
    </body>
</html>







